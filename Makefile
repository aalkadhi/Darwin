CMSSW ?= CMSSW_12_4_0
#Need GCC 11, so export SCRAM_ARCH=slc7_amd64_gcc11
# CMSSW ?= CMSSW_12_4_0
GITLAB ?= https://gitlab.cern.ch
# Get Core from my fork
# ORIGIN ?= $(GITLAB)/DasAnalysisSystem/Core.git
ORIGIN ?= $(GITLAB)/aalkadhi/Core.git
BASE ?= $(PWD)
# Keep the origianl tables
TABLES ?= $(GITLAB)/DasAnalysisSystem/tables.git

.PHONY: all clean

.NOTPARALLEL: all

all: $(CMSSW) libgit2 TUnfold ProtoDarwin tables
	cd $(CMSSW) && \
	eval `scramv1 runtime -sh` && \
	scram b -j 8
#	cd $(CMSSW)/src && \ 
#	source build_and_run.sh

TUnfold: $(CMSSW)
	wget https://www.desy.de/~sschmitt/TUnfold/TUnfold_V17.9.tgz
	mkdir -p TUnfold TUnfold/lib
	mv TUnfold_V17.9.tgz TUnfold
	cd TUnfold && tar xvzf TUnfold_V17.9.tgz
	cd $(CMSSW) && eval `scramv1 runtime -sh` && \
	cd $(BASE)/TUnfold && make lib TUNFOLDVERSION='V17' -j && \
	mv -f libunfold.so lib/libtunfold.so && \
	mv -f TUnfoldV17Dict_rdict.pcm lib/ && \
	cd $(BASE)/$(CMSSW) && scram setup $(BASE)/tunfold.xml

PlottingHelper: $(CMSSW)
	git clone $(GITLAB)/DasAnalysisSystem/PlottingHelper.git
	cd $(CMSSW) && eval `scramv1 runtime -sh` && cd - && \
    make -C PlottingHelper all -j

KinFitter: $(CMSSW)
	# TODO

Teddy:
	# TODO

FastNLO:
	# TODO

#Need GCC 11, so CMSSW_13_2_0_pre2
BOOST=/cvmfs/cms.cern.ch/slc7_amd64_gcc10/external/boost/1.78.0-22078d9ad44a9ee315c3d0995a7d3746

# BOOST=/cvmfs/cms.cern.ch/slc7_amd64_gcc11/external/boost/1.78.0-87d9f13f181dd2996465c6fdbf185bf9
# TODO: improve make statement & better interface with content of xml file (using `xmllint` --> check availability in CI)
ProtoDarwin: $(CMSSW) libgit2 tables
	git clone $(GITLAB)/paconnor/ProtoDarwin.git
	cd $(CMSSW) && eval `scramv1 runtime -sh` && \
	cd $(BASE)/ProtoDarwin && make -j BOOST=$(BOOST) && \
	cd $(BASE)/$(CMSSW) && scram setup $(BASE)/protodarwin.xml

libgit2: $(CMSSW)
	cd $(BASE)/$(CMSSW) && eval `scramv1 runtime -sh` && scram setup $(BASE)/libgit2.xml

$(CMSSW):
	scramv1 project CMSSW $(CMSSW)
	cd $(CMSSW)/src && eval `scramv1 runtime -sh` && \
	git clone https://github.com/cms-jet/JetToolbox.git JMEAnalysis/JetToolbox -b jetToolbox_120X && \
	git clone -b RivetNtuples $(ORIGIN) Core
#	cp Core/RivetNtupliser/data/utils/build_and_run.sh .

tables:
	git clone $(TABLES) $@

clean:
	@rm -rf $(CMSSW) TUnfold ProtoDarwin tables
