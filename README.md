# General instruction

Darwin is a general project involving modular tools for physics analysis. This repository provides the necessary tools to install all pieces of software.

In general, it is recommended that you install the software on a fast disk, but that you process the heavy n-tuples on a dedicated area (e.g. NFS at DESY, EOS at CERN).

## Installation

First source the minimal environment for CMSSW (not necessary at CERN):
```
source setup
```
It is currently working at CERN, at DESY, and for GitLab CI. Feel free to make a merge request to include your favourite facility.

The installation is then trivial:
```
make
```

### Remarks

1. You may overwrite (at your own risks) the CMSSW release by running `make CMSSW=CMSSW_X_Y_Z` where you tune `X`, `Y`, and `Z` to the values of the release that you want to test. This may be necessary to run over certain data sets.
2. `git-cms-init` is not run, since it is a priori not useful for the framework to run, take 100MB of space, and makes the installation slower. If it may happen to be useful, you have to add it in the Makefile. It must be run right after setting up the release, when it is locally still empty.
3. To use SSH instead of HTTPS, run `make GITLAB=ssh://git@gitlab.cern.ch:7999`.

## Setting up the environment

First source the minimal environment for CMSSW as above.  Then just source the CMSSW release as usual with `cmsenv`.

To run CRAB jobs, source the CRAB environment after sourcing the CMSSW environment:
```
source /cvmfs/cms.cern.ch/crab3/crab.sh`
```
Note: you'll need a valid certificate.

### Good practices

Fight against the increase of entropy by
- pulling regularly,
- commiting regularly,
- making regular merge requests.

Happy analysis!
